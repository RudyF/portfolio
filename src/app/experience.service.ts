import { Injectable } from '@angular/core';

import { Experience } from './experience/experience';
import { EXPERIENCES } from './experience/mock-experiences';
import { Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExperienceService {

  constructor() { }

  getExperiences(): Observable<Experience[]> {
    return of(EXPERIENCES);
  }
}
