import { Component, OnInit } from '@angular/core';
import { Skills } from './skills';
import { SkillService } from '../skill.service';
import { ExperienceService } from '../experience.service';
import { Experience } from './experience';
import { Observable } from 'rxjs';
import { FirebaseService } from '../firebase.service';



@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
})
export class ExperienceComponent implements OnInit {
  skills: Skills[] = []; 
  experiences: Experience[] = []; 
  readonly = true;
  accessibility: Skills[];



  constructor(
    private firebaseService : FirebaseService,  
    private skillService: SkillService,
    private experienceService: ExperienceService
    ) {}

    ngOnInit() {
      this.firebaseService.getAccessibility().subscribe(data => {
        this.accessibility = data.map(e => {
          return {
            id: e.payload.doc.id,
            ...e.payload.doc.data()
          } as Skills;
        })
      });
    }
  }