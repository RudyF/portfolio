import { Experience } from './experience';

export const EXPERIENCES: Experience[] = [
    {
        id: 1, 
        role: 'Bilingual Quality Assurance Analyst', 
        company: 'TD Bank Group', 
        date:'Since 2016', 
        city: 'Toronto', 
        province: 'Ontario', 
        country: 'Canada',
        task : 'my task',
        logo: 'assets/images/logo/td.png'
        
    },
    {
        id: 2, 
        role: 'IT Technician', 
        company: 'DisneyLand Paris', 
        date:'2015 to 2016', 
        city: 'Chessy', 
        country: 'France',
        task : 'my taskiii',
        logo: 'assets/images/logo/disneyland.png'
    },
    {
        id: 3, 
        role: 'Globe Job & Role IT User Support Specialist', 
        company: 'Nestle France', 
        date:'2012 to 2014', 
        city: 'Noisiel', 
        country: 'France',
        task: 'my taskkkkkk',
        logo: 'assets/images/logo/nestle.jpeg'
    },
    {
        id: 4, 
        role: 'Accounting Assistant', 
        company: 'Nestle France', 
        date:'2011 to 2012', 
        city: 'Paris', 
        country: 'France',
        task : 'blablabla',
        logo: 'assets/images/logo/nestle.jpeg'
    },
    {
        id: 5, 
        role: 'Accounting Assistant', 
        company: 'Open Conseil', 
        date:'2010 to 2011', 
        city: 'Paris', 
        country: 'France',
        task : 'blablabla',
        logo: 'assets/images/logo/openconseil.jpeg'
    }
]