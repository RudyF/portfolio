export interface Skills {
    id: number,
    name: string;
    rate: number;
}