export interface Experience {
    id: number;
    role: string;
    company: string;
    date: string;
    city: string;
    country: string;
    province?: string;
    logo?: string;
    task?: any;
}