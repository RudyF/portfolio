import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { Skills } from './experience/skills';
import { SKILLS } from './experience/mocK-skills';



@Injectable({
  providedIn: 'root'
})
export class SkillService {

  constructor() { }

  getSkills(): Observable<Skills[]> {
    return of(SKILLS);
  }
}
