// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBKJQPZXS4ZDji5SzWKolTWYADSRpVl4p4",
    authDomain: "portfolio-63fd6.firebaseapp.com",
    databaseURL: "https://portfolio-63fd6.firebaseio.com",
    projectId: "portfolio-63fd6",
    storageBucket: "portfolio-63fd6.appspot.com",
    messagingSenderId: "83806734423",
    appId: "1:83806734423:web:541562c87b79efdd66bb12",
    measurementId: "G-JPTS2Z3B1Y"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
